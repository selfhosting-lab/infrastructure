# selfhosting-lab:infrastructure

Infrastructure provisioning scripts for selfhosting-lab.

Builds a single server running K3s to provide a lightweight self-contained Kubernetes environment for self-hosting
services. The basic host, with K3s installed, runs in about 500MiB of memory, meaning it can run on very small (and
cheap!) infrastructure.

This project exists to experiment with Kubernetes in a simple environment, as well as to provide a reference pipeline
for other projects.

---

## Features

- [K3s](https://k3s.io/)
- [Helm](https://helm.sh/)
- [CoreDNS](https://coredns.io/)
- [Traefik](https://traefik.io/)
- NFS persistent volumes
- Auto-updates for security patches
- User creation
- Well-configured [Fedora 29](https://getfedora.org/) install

---

## Quick start

To get a quick environment up and running on your local machine you can run the following commands:

```shell
bundle install                 # Install dependencies
bundle exec rake kitchen       # Set up environment
bundle exec rake kitchen:login # Login to the environment
```

---

## Requirements

### Local development

- Ruby 2.6.3
- Vagrant

### Deployment to remote infrastructure

- Ruby 2.6.3
- Terraform 0.12.0
- Ansible > 2.6

### Docker

A Docker container is available as an alternative to running on your local machine. This provides a reliable
environment that should work on any host capable of running Docker.

```shell
$ docker run -it --rm -v $(pwd):/builds/ registry.gitlab.com/selfhosting-lab/infrastructure:latest
```

> Note: This Docker container is for use in contributing to the project, not as a runtime for users.

---

## Developing

It's really easy to set up an environment to work with, either locally or with a supported cloud-provider, using
[Kitchen CI](https://kitchen.ci/). Everything you need to do can be carried out through
[Rake](https://ruby.github.io/rake/) tasks which are dynamically created, depending on what environments you have
available to you.

For each environment the following commands are available:

| Name       | Example                         | Description                      |
|------------|---------------------------------|----------------------------------|
|            | `rake kitchen:vagrant`          | Set up environment and run tests |
| `all`      | `rake kitchen:vagrant:all`      | Run End-To-End test cycle        |
| `converge` | `rake kitchen:vagrant:converge` | Deploy configuration             |
| `create`   | `rake kitchen:vagrant:create`   | Create environment               |
| `destroy`  | `rake kitchen:vagrant:destroy`  | Destroy environment              |
| `login`    | `rake kitchen:vagrant:login`    | Interactive shell                |
| `verify`   | `rake kitchen:vagrant:verify`   | Run integration tests            |

You can see the full list of available Rake tasks by simplying running `bundle exec rake`.

### Working locally

You can deploy an instance of this project on your local machine, as long as you have
[Vagrant](https://www.vagrantup.com/) installed.

You can quickly deploy to your local environment by running `bundle exec rake kitchen`.

> As working with Vagrant is the default, you can run any of the commands for Vagrant without specifying the Vagrant
> namespace, so `bundle exec rake kitchen:destroy` is the same as `bundle exec rake kitchen:vagrant:destroy`.

### Working with Digital Ocean

To deploy on Digital Ocean, simply [create an access token](https://cloud.digitalocean.com/account/api/tokens/new),
[upload an SSH key](https://cloud.digitalocean.com/account/security), and follow the steps below.

```shell
export DIGITALOCEAN_ACCESS_TOKEN='<PASTE YOUR ACCESS TOKEN HERE>'
export DIGITALOCEAN_SSH_KEY_IDS='<PASTE THE ID OF YOUR SSH KEY HERE>' # See below if you don't know this.
export DIGITALOCEAN_REGION='lon1' # Set the Digital Ocean region you wish to deploy to.
```

If you don't know your SSH Key ID, you can find out by querying the Digital Ocean API:

```shell
DIGITALOCEAN_ACCESS_TOKEN='<PASTE YOUR ACCESS TOKEN HERE>'
DIGITALOCEAN_SSH_KEY='<NAME OF YOUR SSH KEY IN DIGITAL OCEAN>'
DIGITALOCEAN_SSH_KEY_IDS=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${DIGITALOCEAN_ACCESS_TOKEN}"  "https://api.digitalocean.com/v2/account/keys" | jq --arg key ${DIGITALOCEAN_SSH_KEY} '.ssh_keys | .[] | select(.name == $key) | .id')
echo "Your token ID is '${DIGITALOCEAN_SSH_KEY_IDS}'"
export DIGITALOCEAN_ACCESS_TOKEN
export DIGITALOCEAN_SSH_KEY_IDS
```

If you've set up your access token in your environment you should now see Digital Ocean listed in the Rake tasks if you
run `bundle exec rake`.

You can quickly deploy to Digital Ocean by running `bundle exec rake kitchen:digitalocean`.

---

## Testing

### Unit testing

Unit tests should be run frequently to check for syntax and styling issues whilst developing. They are also used as a
quality gate early in the CI/CD pipeline.

Unit test are available to test the syntax and style guidelines for:
 - Ruby code
 - Terraform modules
 - Ansible playbooks and roles
 - YAML files

Available unit test tasks:
```shell
rake test:unit                   # Test - Unit tests - All tests
rake test:unit:lint              # Test - Unit tests - Linting checks - All tests
rake test:unit:lint:ruby         # Test - Unit tests - Linting checks - Ruby code
rake test:unit:lint:terraform    # Test - Unit tests - Linting checks - Terraform code
rake test:unit:lint:yaml         # Test - Unit tests - Linting checks - YAML files
rake test:unit:syntax            # Test - Unit tests - Syntax checks - All tests
rake test:unit:syntax:ansible    # Test - Unit tests - Syntax checks - Ansible files
rake test:unit:syntax:ruby       # Test - Unit tests - Syntax checks - Ruby code
rake test:unit:syntax:terraform  # Test - Unit tests - Syntax checks - Terraform files
rake test:unit:syntax:yaml       # Test - Unit tests - Syntax checks - YAML files
```

### Integration testing

Integration tests are necessary to verify an installation is set up correctly.
JUnit results for Integration tests are stored in the `results/` directory.

You can also provision an accurate environment, configure it, and then run the test suite. This is the same as the
Kitchen CI process except performed on remote infrastructure which should be close to identical to the production
environment.

Available integration test tasks:
```shell
rake test:integration            # Test - Integration tests - Full lifecycle test
rake test:integration:configure  # Test - Integration tests - Configure environment
rake test:integration:create     # Test - Integration tests - Create environment
rake test:integration:destroy    # Test - Integration tests - Destroy environment
rake test:integration:verify     # Test - Integration tests - Verify environment
```
