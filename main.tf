provider digitalocean {
  version = "~> 1.7"
}

variable "name" {
  type        = "string"
  description = "Build name"
  default     = "local-test"
}

variable "ssh_public_key" {
  type        = "string"
  description = "SSH Public Key path"
  default     = "./builds/artefacts/id_rsa.pub"
}

variable "monitoring" {
  type        = "string"
  description = "Enable detailed monitoring"
  default     = "false"
}

resource "digitalocean_ssh_key" "main" {
  name       = "${var.name}"
  public_key = "${file("${var.ssh_public_key}")}"
}

resource "digitalocean_droplet" "main" {
  name       = "${var.name}"
  monitoring = "${var.monitoring}"
  image      = "fedora-29-x64"
  region     = "lon1"
  size       = "s-1vcpu-1gb"

  ssh_keys = [
    "${digitalocean_ssh_key.main.id}",
  ]

  tags = [
    "ci",
    "test",
  ]
}

output "ip_address" {
  value = "${digitalocean_droplet.main.ipv4_address}"
}
