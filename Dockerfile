FROM ruby:2.6.3
LABEL maintainer='ben.fairless@gmail.com'
LABEL description='CI container for selfhosting-lab/infrastructure'
LABEL url='registry.gitlab.com/selfhosting-lab/infrastructure'

ARG USER='user'
ENV HOME="/home/${USER}"
ENV BUNDLE_PATH="${HOME}/.bundle"
ENV GEM_HOME="${HOME}/.bundle"
ENV ANSIBLE_VERSION='2.8.4'
ENV TERRAFORM_VERSION='0.12.8'
ENV RUBY_VERSION='2.6.3'

# Create GitLab builds directory
RUN mkdir /builds && chown 777 /builds

# Set up user
RUN groupadd -g 990 ${USER} &&\
    useradd -r -u 990 -g ${USER} ${USER} -m -d ${HOME} &&\
    mkdir -p ${HOME}/.ssh &&\
    chown -R ${USER}:${USER} ${HOME} &&\
    chmod 0700 ${HOME}/.ssh &&\
    echo "PATH=${BUNDLE_PATH}/bin:\$PATH" > ${HOME}/.bashrc

# Install Ansible
RUN apt-get update &&\
    apt-get install python3 python3-pip sshpass -y &&\
    apt-get clean &&\
    pip3 install ansible=="${ANSIBLE_VERSION}"

# Install Terraform
RUN curl -L -o /tmp/terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip &&\
    unzip -q /tmp/terraform.zip -d /usr/bin/ &&\
    rm -f /tmp/terraform.zip

# Install packages
COPY Gemfile /builds/Gemfile
RUN bundle install --clean --gemfile /builds/Gemfile && rm -f /builds/Gemfile && chown -R ${USER}:${USER} ${HOME}/.bundle

# Runtime config
USER user
WORKDIR /builds
CMD ["/bin/bash", "-l"]
