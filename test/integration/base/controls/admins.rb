title 'admins'

control 'admins' do
  impact 0.3
  title 'Administrators are configured correctly'
  describe group('admin') do
    it { should exist }
  end
  describe file('/etc/sudoers.d/admin') do
    it { should exist }
  end
end
