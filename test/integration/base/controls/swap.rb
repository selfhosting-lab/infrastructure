title 'swap'

control 'swap' do
  impact 0.8
  title 'Swap file is enabled'
  describe command('/usr/sbin/swapon -s') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match(/file/m) }
  end
end
