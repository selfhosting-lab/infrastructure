title 'time'

control 'time' do
  impact 0.8
  title 'Timezone and time synchronisation configured correctly'
  describe package('chrony') do
    it { should be_installed }
  end
  describe systemd_service('chronyd') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe file('/etc/localtime') do
    its('link_path') { should eq '/usr/share/zoneinfo/Europe/London' }
  end
end
