title 'auto update'

control 'autoupdate' do
  impact 0.8
  title 'Automatic security updates configured correctly'
  describe package('dnf-automatic') do
    it { should be_installed }
  end
  describe systemd_service('dnf-automatic.timer') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
