title 'available'

control 'available' do
  impact 0.3
  title 'Ensure NFS storage class is available'
  namespace = 'kube-storage'

  describe command('/usr/local/bin/k3s kubectl get storageclass/remote-persistent --no-headers=true') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match(/\(default\)/m) }
  end
  describe command("/usr/local/bin/k3s kubectl -n #{namespace} get service/nfs-provisioner") do
    its('exit_status') { should eq 0 }
  end
  # Can't do a normal port check as the IP needs to be looked up host side
  describe command("rpcinfo -t $(kubectl -n #{namespace} get service/nfs-provisioner -o=jsonpath='{.spec.clusterIP}') nfs 4") do
    its('exit_status') { should eq 0 }
    its('stdout') { should match(/ready and waiting/m) }
  end
end
