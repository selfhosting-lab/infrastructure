title 'nfs'

control 'nfs' do
  impact 0.3
  title 'Ensure NFS service is set up correctly'
  namespace = 'kube-storage'
  describe command("/usr/local/bin/k3s kubectl -n #{namespace} get statefulset/nfs-provisioner") do
    its('exit_status') { should eq 0 }
  end
  describe command("/usr/local/bin/k3s kubectl -n #{namespace} get pvc/nfs-provisioner") do
    its('exit_status') { should eq 0 }
  end
end
