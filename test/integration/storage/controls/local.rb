title 'local storage'

control 'local storage' do
  impact 0.3
  title 'Ensure local storage is set up correctly'
  describe command('/usr/local/bin/k3s kubectl get pv/local-volume01') do
    its('exit_status') { should eq 0 }
  end
  describe command('/usr/local/bin/k3s kubectl get storageclass/local-storage') do
    its('exit_status') { should eq 0 }
  end
end
