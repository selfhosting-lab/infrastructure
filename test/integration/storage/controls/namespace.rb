title 'namespace'

control 'namespace' do
  impact 0.3
  title 'Ensure namespace is set up correctly'
  describe command('/usr/local/bin/k3s kubectl get ns/kube-storage') do
    its('exit_status') { should eq 0 }
  end
end
