title 'tiller'

control 'tiller' do
  impact 0.8
  title 'Ensure Tiller is installed correctly'

  describe command('/usr/local/bin/k3s kubectl get deployment/tiller -n kube-system -o custom-columns=:.status.readyReplicas --no-headers') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match '1' }
  end
  describe command('/usr/local/bin/k3s kubectl get service/tiller -n kube-system') do
    its('exit_status') { should eq 0 }
  end
end
