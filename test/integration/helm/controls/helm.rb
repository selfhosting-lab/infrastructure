title 'helm'

control 'helm' do
  impact 0.8
  title 'Ensure Helm is installed correctly'

  describe command('helm') do
    it { should exist }
  end
  describe command('helm list') do
    its('exit_status') { should eq 0 }
  end
end
