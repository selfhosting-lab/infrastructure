title 'selinux'

control 'selinux' do
  impact 1.0
  title 'selinux is properly configured'
  describe command('getenforce') do
    it { should exist }
    its('stdout') { should match 'Enforcing' }
  end
end
