title 'iptables'

control 'iptables' do
  impact 0.8
  title 'iptables is properly configured'
  describe systemd_service('iptables') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe iptables(table: 'filter', chain: 'INPUT') do
    it { should have_rule('-P INPUT DROP') }
  end
  describe iptables(table: 'filter', chain: 'FORWARD') do
    it { should have_rule('-P FORWARD DROP') }
  end
  describe iptables(table: 'filter', chain: 'INPUT') do
    it { should have_rule('-A INPUT -p icmp -j ACCEPT') }
  end
  describe iptables(table: 'filter', chain: 'INPUT') do
    it { should have_rule('-A INPUT -i lo -j ACCEPT') }
  end
end
