title 'running'

control 'running' do
  impact 0.9
  title 'Traefik is running'
  describe command('/usr/local/bin/k3s kubectl -n kube-ingress get deployment/traefik -o custom-columns=:.status.readyReplicas --no-headers') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match '1' }
  end
end
