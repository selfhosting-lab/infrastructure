title 'installation'

control 'installation' do
  impact 1.0
  title 'Traefik is installed'
  describe command('/usr/local/bin/k3s kubectl get deployment/traefik -n kube-ingress') do
    its('exit_status') { should eq 0 }
  end
  describe command('/usr/local/bin/k3s kubectl get service/traefik -n kube-ingress') do
    its('exit_status') { should eq 0 }
  end
  describe command('helm list traefik --output yaml') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match(/Status: DEPLOYED/m) }
  end
end
