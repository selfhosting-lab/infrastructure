title 'available'

control 'available' do
  impact 0.8
  title 'Traefik is listening and available externally'
  describe http('http://localhost:80/',
    method:        'GET',
    open_timeout:  60,
    read_timeout:  60,
    ssl_verify:    false,
    max_redirects: 0) do
    its('status') { should eq 301 }
    its('body') { should eq 'Moved Permanently' }
    its('headers.Location') { should eq 'https://localhost/' }
  end
  describe http('https://localhost:443/',
    method:        'GET',
    open_timeout:  60,
    read_timeout:  60,
    ssl_verify:    false,
    max_redirects: 3) do
    its('status') { should eq 404 }
    its('body') { should eq '404 page not found' }
  end
end
