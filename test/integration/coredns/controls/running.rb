title 'running'

control 'running' do
  impact 0.9
  title 'CoreDNS is running'
  describe command('/usr/local/bin/k3s kubectl -n kube-system get deployment/coredns -o custom-columns=:.status.readyReplicas --no-headers') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match '1' }
  end
end
