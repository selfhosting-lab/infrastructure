title 'available'

control 'available' do
  impact 0.8
  title 'CoreDNS is able to resolve addresses'
  clusterip = "$(kubectl -n kube-system get service/kube-dns -o=jsonpath='{.spec.clusterIP}')"
  describe command("dig +short A localhost @#{clusterip}") do
    its('exit_status') { should eq 0 }
    its('stdout') { should match '127.0.0.1' }
  end
end
