title 'installation'

control 'installation' do
  impact 1.0
  title 'CoreDNS is installed'
  describe command('/usr/local/bin/k3s kubectl get deployment/coredns -n kube-system') do
    its('exit_status') { should eq 0 }
  end
  describe command('/usr/local/bin/k3s kubectl get service/kube-dns -n kube-system') do
    its('exit_status') { should eq 0 }
  end
end
