title 'installation'

control 'installation' do
  impact 1.0
  title 'K3s is installed'
  describe systemd_service('k3s') do
    it { should be_installed }
  end
  describe command('k3s') do
    it { should exist }
  end
  describe file('/etc/sysconfig/k3s') do
    it { should exist }
  end
  describe file('/etc/rancher/k3s/certificate.pem') do
    its('content') { should match(/-{5}BEGIN CERTIFICATE-{5}.*-{5}END CERTIFICATE-{5}/m) }
  end
end
