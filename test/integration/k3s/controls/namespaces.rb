title 'namespaces'

namespaces = %w[selfhosting-live selfhosting-staging selfhosting-development]
namespaces.each do |namespace|
  control "namespaces - #{namespace}" do
    impact 0.3
    title 'Ensure namespace is set up properly'
    describe command("/usr/local/bin/k3s kubectl get ns/#{namespace} -o name") do
      its('exit_status') { should eq 0 }
      its('stdout') { should match "namespace/#{namespace}" }
    end
    describe command("/usr/local/bin/k3s kubectl get sa/#{namespace}-deploy -n #{namespace} -o name") do
      its('exit_status') { should eq 0 }
      its('stdout') { should match "serviceaccount/#{namespace}" }
    end
  end
end
