title 'available'

control 'available' do
  impact 0.8
  title 'K3s is able to communicate with client'
  describe command('/usr/local/bin/k3s kubectl version') do
    its('exit_status') { should eq 0 }
    its('stdout') { should match 'Server Version' }
  end

  # Load k3s authentication
  config = YAML.safe_load(file('/etc/rancher/k3s/k3s.yaml').content)
  credentials = config['users'][0]['user']

  describe http('https://localhost:6443/healthz',
    method:        'GET',
    open_timeout:  60,
    read_timeout:  60,
    ssl_verify:    false,
    max_redirects: 3,
    auth:          {
      user: credentials['username'],
      pass: credentials['password']
    }) do
    its('status') { should eq 200 }
    its('body') { should eq 'ok' }
  end
end
