title 'running'

control 'running' do
  impact 0.9
  title 'K3s is running'
  describe systemd_service('k3s') do
    it { should be_enabled }
    it { should be_running }
  end
  describe port(6443) do
    it { should be_listening }
    its('protocols') { should include 'tcp' }
    its('addresses') { should include '0.0.0.0' }
  end
end
