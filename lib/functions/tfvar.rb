require 'open3'

def tfvar(var)
  # Read variable from Terraform
  _, content = Open3.popen3("terraform output #{var}")
  return content.read.chomp!
end
