require 'open3'
require_relative 'naming'

# Run a command, optionally silently
def run(command, silent: false)
  STDOUT.sync = true
  status      = 1 # Exit status
  env         = {
    'TF_IN_AUTOMATION' => 'true', # Default Terraform automation variable
    'TF_INPUT'         => 'false', # Do not prompt for input in Terraform
    'TF_LOG_PATH'      => "#{build_path}/terraform.log"
  }

  Open3.popen3(env, command) do |_stdin, stdout, stderr, thread|
    unless silent
      [stdout, stderr].each do |stream|
        Thread.new do
          # Read until stream closes
          until (line = stream.gets).nil?
            puts line.to_s
          end
        end
      end
    end
    thread.join # Wait for process to complete
    status = thread.value.exitstatus # Record exit status to return
  end
  return status
end
