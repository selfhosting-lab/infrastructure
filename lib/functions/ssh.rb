require 'sshkey'

# Manage CI keys
class CIKey
  attr_accessor :name, :path, :public

  def initialize(params = {})
    @name   = params.fetch(:name, 'ci-temporary-key-' + Time.now.getutc.to_s)
    @path   = params.fetch(:path, Dir.pwd + '/id_rsa')
    @public = params.fetch(:public, "#{@path}.pub")

    # Create SSH Key pair
    keypair = SSHKey.generate(
      type:    'RSA',
      bits:    params.fetch(:bits, 4096),
      comment: @name
    )
    @private_key = keypair.private_key
    @public_key  = keypair.ssh_public_key
  end

  def create
    File.write(@path, @private_key) unless File.file?(@path)
    File.write(@public, @public_key) unless File.file?(@public)
    File.chmod(0600, @path)
    File.chmod(0644, @public)
  end

  def delete
    [@path, @public].each do |f|
      File.delete(f) if File.exist?(f)
    end
  end
end
