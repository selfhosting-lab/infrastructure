require 'fileutils'

def build_path
  # Generate sensible paths for artefacts based on the current job or time
  path = Dir.pwd + '/artefacts'
  # Make sure directory exists
  FileUtils.mkdir_p path
  return path
end

def build_name
  # Generate sensible names for artefacts based on the current pipeline or time
  prefix = 'ci'

  # GitLab integration
  return "#{prefix}-#{ENV['CI_PROJECT_NAME']}-#{ENV['CI_PIPELINE_ID']}" if ENV['GITLAB_CI']

  # Local build
  return "#{prefix}-local-" + Time.now.getutc.strftime('%Y%m%d-%H%M%S')
end
