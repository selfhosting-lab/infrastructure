desc 'Test - Integration tests - Full lifecycle test'
task integration: 'integration:all'
namespace :integration do
  Dir.glob(File.dirname(__FILE__) + '/integration/*.rake').each { |r| load r }
end
