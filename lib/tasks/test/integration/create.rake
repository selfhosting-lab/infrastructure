require 'colorize'
require 'json'
require_relative '../../../functions/naming'
require_relative '../../../functions/ssh'
require_relative '../../../functions/run'

desc 'Test - Integration tests - Create environment'
task :create do
  Rake::Task['test:integration:setup'].invoke
  name     = build_name
  var_file = "#{build_path}/variables.json"
  command = "terraform apply -var-file #{var_file} -auto-approve"
  # command = "terraform plan -var-file #{var_file}"

  ssh_key = CIKey.new(path: "#{build_path}/id_rsa")
  ssh_key.create

  # Generate variables for Terraform and save them to file
  vars = {
    'name'            => name,
    'ssh_public_key'  => ssh_key.public,
    'ssh_private_key' => ssh_key.path,
    'inventory'       => "#{build_path}/inventory.yml",
    'user'            => 'root'
  }
  File.open(var_file, 'w') do |f|
    f.write(JSON.pretty_generate(vars))
  end

  # Run Terraform
  puts 'Terraform will now provision all resources.'.colorize(:blue)
  process = run(command)

  # Check status
  if process.zero?
    puts 'All resources provisioned successfully.'.colorize(:green)
  else
    abort 'Not all resources could be provisioned.'.colorize(:red)
  end
end
