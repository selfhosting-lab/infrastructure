require 'colorize'
require 'json'
require 'open3'
require_relative '../../../functions/naming'
require_relative '../../../functions/run'
require_relative '../../../functions/tfvar'

desc 'Test - Integration tests - Verify environment'
task :verify do
  vars    = JSON.parse(File.read("#{build_path}/variables.json"))
  ssh_key = vars['ssh_private_key']
  ssh_uri = "ssh://#{vars['user']}@#{tfvar('ip_address')}"
  suite   = 'test/integration/default'
  command = "inspec exec #{suite} --target #{ssh_uri} -i #{ssh_key} --reporter=cli junit:results/inspec.xml --chef-license=accept-silent"

  # Run Ansible
  puts 'InSpec will now verify all resources.'.colorize(:blue)
  sleep(60)
  process = run(command)

  # Check status
  if process.zero?
    puts 'All resources verified successfully.'.colorize(:green)
  else
    abort 'Not all resources could be verified.'.colorize(:red)
  end
end
