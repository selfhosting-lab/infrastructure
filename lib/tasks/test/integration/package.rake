require 'colorize'
require 'open3'

# desc 'Test - Integration tests - Package tests'
task :package do
  label    = 'InSpec package -'
  profiles = Dir['test/integration/*/']
  args     = '--overwrite --chef-license=accept-silent'
  failure  = false

  # Loop through profiles and perform packaging if necessary
  profiles.each do |profile|
    # unless File.file?("#{profile}/inspec.lock")
    stdout, thread = Open3.capture2("inspec vendor #{profile} #{args}")

    # Check if command exits with non-zero status
    next if thread.success?

    begin
      result = stdout
      puts "#{label} #{profile} failed".colorize(:light_red)
      puts result
    rescue RuntimeError => e
      puts "#{label} Error reading output of #{file}".colorize(:red)
      puts(e.to_s)
    end
    failure = true
    # end

    # Report failure
    abort "#{label} failed to package some tests.".colorize(:red) if failure
  end
end
