require 'colorize'

task :all do
  begin
    Rake::Task['test:integration:create'].invoke
    Rake::Task['test:integration:configure'].invoke
    Rake::Task['test:integration:verify'].invoke
  rescue SystemExit
    failed = true
  end

  Rake::Task['test:integration:destroy'].invoke

  abort 'Some steps failed!'.colorize(:red) if failed
end
