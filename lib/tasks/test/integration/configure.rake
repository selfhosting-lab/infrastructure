require 'colorize'
require 'json'
require 'open3'
require_relative '../../../functions/naming'
require_relative '../../../functions/run'
require_relative '../../../functions/tfvar'

desc 'Test - Integration tests - Configure environment'
task :configure do
  vars           = JSON.parse(File.read("#{build_path}/variables.json"))
  inventory_file = vars['inventory']
  playbook       = 'playbooks/integration.yaml'
  command        = "ansible-playbook -i #{inventory_file} #{playbook}"

  inventory = {
    'all' => {
      'hosts' => {
        vars['name'].to_s => {
          'ansible_ssh_host'             => tfvar('ip_address').to_s,
          'ansible_ssh_user'             => vars['user'],
          'ansible_ssh_private_key_file' => vars['ssh_private_key'],
          'ansible_python_interpreter'   => '/usr/bin/python3'
        }
      }
    }
  }
  File.write(inventory_file, inventory.to_yaml)

  # Run Ansible
  puts 'Ansible will now configure all resources.'.colorize(:blue)
  process = run(command)

  # Check status
  if process.zero?
    puts 'All resources configured successfully.'.colorize(:green)
  else
    abort 'Not all resources could be configured.'.colorize(:red)
  end
end
