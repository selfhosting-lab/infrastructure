require 'colorize'
require 'open3'

# desc 'Test - Integration tests - Setup Terraform'
task :setup do
  label   = 'Terraform setup -'
  command = 'terraform init -backend=false'

  # Perform syntax check
  _stdin, _stdout, stderr, thread = Open3.popen3(command)

  # Output STDERR
  until (line = stderr.gets).nil?
    line = line.chomp.gsub(/\e\[(\d+)m/, '') # Strip control characters
    puts "#{label} #{line}".colorize(:light_red) if line.length.positive?
  end

  # Check if command exits with non-zero status
  abort unless thread.value.success?
end
