require 'colorize'
require 'open3'
require 'rake'
require_relative '../../../functions/run'

desc 'Test - Integration tests - Destroy environment'
task :destroy do
  Rake::Task['test:integration:setup'].invoke
  var_file     = "#{build_path}/variables.json"
  grace_period = 5
  command      = "terraform destroy -var-file #{var_file} -auto-approve"

  # Since this is a dangerous task, allow a grace period for the user to cancel
  puts "Terraform will start to destroy resources in #{grace_period} seconds.".colorize(:red)
  sleep(grace_period)
  puts 'Terraform will now destroy all resources.'.colorize(:light_red)

  process = run(command)

  # Check status
  if process.zero?
    puts 'All resources destroyed successfully.'.colorize(:green)
  else
    abort 'Not all resources could be destroyed.'.colorize(:red)
  end
end
