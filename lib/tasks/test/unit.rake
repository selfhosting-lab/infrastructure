desc 'Test - Unit tests - All tests'
task unit: 'unit:all'
namespace :unit do |n|
  Dir.glob(File.dirname(__FILE__) + '/unit/*.rake').each { |r| load r }
  multitask all: n.tasks
end
