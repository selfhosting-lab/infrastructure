desc 'Test - Unit tests - Syntax checks - All tests'
task syntax: 'syntax:all'
namespace :syntax do |n|
  Dir.glob(File.dirname(__FILE__) + '/syntax/*.rake').each { |r| load r }
  multitask all: n.tasks
end
