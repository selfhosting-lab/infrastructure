require 'colorize'
require 'open3'
require_relative '../../../../functions/naming'

desc 'Test - Unit tests - Syntax checks - Ansible files'
task :ansible do
  failure   = false
  label     = 'Ansible syntax -'
  inventory = "/tmp/#{build_name}-inventory"
  args      = "--syntax-check -i #{inventory}"
  files     = Dir['playbooks/*.yml', 'playbooks/*.yaml'] # Playbooks to check

  File.write(inventory, 'localhost')
  # Loop through files and perform syntax check
  files.each do |file|
    _stdin, _stdout, stderr, thread = Open3.popen3("ansible-playbook #{file} #{args}")

    # Check if command exits with non-zero status
    next if thread.value.success?

    begin
      result = stderr.gets.chomp.sub(' syntax error:', '')
      puts "#{label} [#{file}] #{result}".colorize(:red)
    rescue RuntimeError => e
      puts "#{label} Error reading output of #{file}".colorize(:red)
      puts(e.to_s)
    end
    failure = true
  end

  File.delete(inventory)

  # Report success
  if failure
    abort
  else
    puts "#{label} syntax check passed.".colorize(:green)
  end
end
