require 'colorize'
require 'open3'

desc 'Test - Unit tests - Syntax checks - Terraform files'
task :terraform do
  Rake::Task['test:integration:setup'].invoke
  label   = 'Terraform syntax -'
  command = 'terraform validate'

  # Perform syntax check
  _stdin, _stdout, stderr, thread = Open3.popen3(command)

  # Output STDERR
  until (line = stderr.gets).nil?
    line = line.chomp.gsub(/\e\[(\d+)m/, '') # Strip control characters
    puts "#{label} #{line}".colorize(:light_red) if line.length.positive?
  end

  # Check if command exits with non-zero status
  if thread.value.success?
    puts "#{label} syntax check passed.".colorize(:green)
  else
    abort
  end
end
