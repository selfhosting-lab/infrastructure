require 'colorize'
require 'open3'

desc 'Test - Unit tests - Syntax checks - Ruby code'
task :ruby do
  failure = false
  label   = 'Ruby syntax -'
  files   = Dir['**/*.rb', '**/*.rake', 'Rakefile', 'Gemfile'] # Files to check

  # Loop through files and perform syntax check
  files.each do |file|
    _stdin, _stdout, stderr, thread = Open3.popen3("ruby -c #{file}")

    # Check if command exits with non-zero status
    next if thread.value.success?

    begin
      result = stderr.gets.chomp.sub(' syntax error:', '')
      puts "#{label} #{result}".colorize(:red)
    rescue RuntimeError => e
      puts "#{label} Error reading output of #{file}".colorize(:red)
      puts(e.to_s)
    end
    failure = true
  end

  # Report success
  if failure
    abort
  else
    puts "#{label} syntax check passed.".colorize(:green)
  end
end
