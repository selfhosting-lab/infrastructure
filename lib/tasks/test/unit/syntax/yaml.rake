require 'colorize'
require 'yaml'

desc 'Test - Unit tests - Syntax checks - YAML files'
task :yaml do
  failure = false
  label   = 'YAML syntax -'
  types   = Dir['**/*.yaml', '**/*.yml'] # File types to check

  # Ignore kitchen.yml as it is not actually a YAML file, but instead is an ERB template
  files = FileList.new(types) { |fl| fl.exclude(/kitchen\.yml/) }

  # Loop through files and perform syntax check
  files.each do |file|
    YAML.load_file(file)
  rescue StandardError
    puts "#{label} Failed to read #{file}: #{$ERROR_INFO}".colorize(:red)
    failure = true
  end

  # Report success
  if failure
    abort
  else
    puts "#{label} syntax check passed.".colorize(:green)
  end
end
