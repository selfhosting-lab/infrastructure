require 'rubocop/rake_task'

desc 'Test - Unit tests - Linting checks - Ruby code'
task :ruby do
  label   = 'Ruby linting -'
  command = 'rubocop -PE -f clang'

  _stdin, stdout, _stderr, thread = Open3.popen3(command)
  output = stdout.read.strip

  # Abort and report if Rubocop linter cannot run
  unless thread.value.success?
    puts output.uncolorize.colorize(:light_red)
    abort "#{label} Linting check found issues.".colorize(:red)
  end
  puts "#{label} linting check passed.".colorize(:green)
end
