require 'colorize'
require 'yamllint'
require 'rake/tasklib'

desc 'Test - Unit tests - Linting checks - YAML files'
task :yaml do
  label  = 'YAML linting -'
  types  = ['**/*.yaml', '**/*.yml'] # File types to check

  # Ignore kitchen.yml as it is not actually a YAML file, but instead is an ERB template
  files = FileList.new(types) { |fl| fl.exclude(/kitchen\.yml/) }

  # Lint appropriate files
  linter = ::YamlLint::Linter.new(disable_ext_check: false)
  linter.check_all(files)

  # Report linter errors
  if linter.errors?
    puts "YAML lint found #{linter.errors_count} issues:".colorize(:red)
    linter.errors.each do |path, errors|
      puts path.colorize(:light_red)
      errors.each do |err|
        puts "  #{err}".colorize(:yellow)
      end
    end
    abort
  else
    puts "#{label} linting check passed.".colorize(:green)
  end
end
