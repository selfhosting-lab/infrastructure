require 'colorize'
require 'open3'

desc 'Test - Unit tests - Linting checks - Terraform code'
task :terraform do
  label = 'Terraform linting -'
  command = 'terraform fmt -write=true -diff=true'
  _stdin, stdout, stderr, thread = Open3.popen3(command)
  output = stdout.read.strip

  # Abort and report if Terraform linter cannot run
  unless thread.value.success?
    puts stderr.read.strip.uncolorize.colorize(:light_red)
    abort "#{label} Unable to complete linting due to syntax errors!".colorize(:red)
  end

  # Report on any linter findings
  if output.length.positive?
    puts "#{label} found (and fixed) linting issues:".colorize(:light_red)
    puts output.uncolorize.colorize(:yellow)
  # Report success
  else
    puts "#{label} linting check passed.".colorize(:green)
  end
end
