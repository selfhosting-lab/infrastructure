require 'colorize'
require 'open3'

# desc 'Test - Unit tests - Linting checks - InSpec integration tests'
task :inspec do
  Rake::Task['test:integration:package'].invoke
  label    = 'InSpec linting -'
  args     = '--chef-license=accept-silent'
  profiles = Dir['test/integration/*/']
  failure  = false

  # Loop through profiles and perform lint check
  profiles.each do |profile|
    stdout, thread = Open3.capture2("inspec check #{profile} #{args}")

    # Check if command exits with non-zero status
    next if thread.success?

    begin
      result = stdout
      puts "#{label} #{profile}".colorize(:light_red)
      puts result
    rescue RuntimeError => e
      puts "#{label} Error reading output of #{file}".colorize(:red)
      puts(e.to_s)
    end
    failure = true
  end

  # Report success
  if failure
    abort "#{label} Linting check found issues.".colorize(:red)
  else
    puts "#{label} linting check passed.".colorize(:green)
  end
end
