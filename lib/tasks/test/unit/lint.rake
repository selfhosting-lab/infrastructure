desc 'Test - Unit tests - Linting checks - All tests'
task lint: 'lint:all'
namespace :lint do |n|
  Dir.glob(File.dirname(__FILE__) + '/lint/*.rake').each { |r| load r }
  multitask all: n.tasks
end
