# Default 'test' job runs the Unit suite as that's as far as can be run safely
desc 'Test - Run default (unit) suite'
task test: 'test:unit'
namespace :test do
  Dir.glob(File.dirname(__FILE__) + '/test/*.rake').each { |r| load r }
end
