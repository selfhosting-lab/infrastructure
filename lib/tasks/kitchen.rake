require 'kitchen'

kitchen_description = 'Kitchen CI'
kitchen_namespace   = 'kitchen'
default_platform    = 'vagrant'
kitchen_actions     = {
  'create':   'Create environment',
  'converge': 'Deploy configuration',
  'verify':   'Run integration tests',
  'destroy':  'Destroy environment',
  'login':    'Interactive shell'
}

# Alias task to run default platform setup
desc "#{kitchen_description} - Set up local development environment (#{default_platform.capitalize})"
task kitchen_namespace => "#{kitchen_namespace}:#{default_platform}"

namespace kitchen_namespace do
  # Create aliases to tasks for the default platform
  kitchen_actions.each do |action, _|
    task action => "#{kitchen_namespace}:#{default_platform}:#{action}"
  end

  # Dynamically create tasks for each Kitchen CI platform
  platforms = Kitchen::Config.new.platforms.map.each(&:name)
  platforms.each do |platform|
    description = "#{kitchen_description} - #{platform.capitalize} environment"

    # Create setup task
    desc "#{description} - Set up environment and run tests"
    task platform do
      Rake::Task["#{kitchen_namespace}:#{platform}:create"].invoke
      Rake::Task["#{kitchen_namespace}:#{platform}:converge"].invoke
      Rake::Task["#{kitchen_namespace}:#{platform}:verify"].invoke
    end

    # Create namespace with dynamic tasks
    namespace platform do
      # Create tasks for each default Kitchen CI action
      kitchen_actions.each do |action, explanation|
        desc "#{description} - #{explanation}"
        task action do
          begin
            instances = Kitchen::Config.new.instances.find_all { |instance| instance.platform.name == platform }
            instances.each { |instance| instance.send(action) }
          rescue => hmm
            puts "#{hmm}"
          end
        end
      end

      # Create 'all' task for running all other tasks in sequence
      desc "#{description} - Run End-To-End test cycle"
      task :all do
        Rake::Task["#{kitchen_namespace}:#{platform}:create"].invoke
        Rake::Task["#{kitchen_namespace}:#{platform}:converge"].invoke
        Rake::Task["#{kitchen_namespace}:#{platform}:verify"].invoke
        Rake::Task["#{kitchen_namespace}:#{platform}:destroy"].invoke
      end
    end
  end
end
