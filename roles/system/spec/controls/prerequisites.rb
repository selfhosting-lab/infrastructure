# Task file for tests
ref_file = 'roles/system/tasks/prerequisites.yaml'

control 'prerequisites-01' do
  title 'Ensure prerequisite packages are installed'
  impact 'low'
  ref ref_file
  packages = %w[kernel python2-libselinux python3-libselinux]
  packages.each do |rpm|
    describe package(rpm) do
      it { should be_installed }
    end
  end
end
