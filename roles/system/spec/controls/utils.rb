# Task file for tests
ref_file = 'roles/system/tasks/utils.yaml'

control 'utils-01' do
  title 'Ensure utility packages are installed'
  impact 'low'
  ref ref_file
  packages = %w[
    bash-completion bind-utils curl git glances htop httpd-tools iotop iperf3
    kexec-tools lsof nfs-utils nmap-ncat perf pv realmd screen socat stress-ng
    tcpdump vim-enhanced wget
  ]
  packages.each do |rpm|
    describe package(rpm) do
      it { should be_installed }
    end
  end
end
