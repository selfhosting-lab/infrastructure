# Task file for tests
ref_file = 'roles/system/tasks/tuning.yaml'

control 'tuning-01' do
  title 'Ensure the system will restart if the kernel panics'
  impact 'high'
  ref ref_file
  describe kernel_parameter('kernel.panic') do
    its('value') { should eq 10 }
  end
end

control 'tuning-02' do
  title 'Ensure the tuned packages are installed'
  impact 'medium'
  ref ref_file
  packages = %w[tuned-utils tuned-profiles-atomic]
  packages.each do |rpm|
    describe package(rpm) do
      it { should be_installed }
    end
  end
end
