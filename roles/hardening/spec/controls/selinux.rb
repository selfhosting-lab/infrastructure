# Task file for tests
ref_file = 'roles/hardening/tasks/selinux.yaml'

control 'selinux-01' do
  title 'Ensure SELinux python library is installed'
  impact 'medium'
  ref ref_file
  packages = %w[python2-libselinux python3-libselinux]
  packages.each do |rpm|
    describe package(rpm) do
      it { should be_installed }
    end
  end
end

control 'selinux-02' do
  title 'Ensure SELinux is enforcing'
  impact 'critical'
  ref ref_file
  describe command('getenforce') do
    its('stdout') { should match 'Enforcing' }
  end
end
